<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_J2bulkorder
 * @author     JooMonk <support@joomonk.com>
 * @copyright  JooMonk
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

// Access check.
if (!JFactory::getUser()->authorise('core.manage', 'com_j2bulkorder'))
{
	throw new Exception(JText::_('JERROR_ALERTNOAUTHOR'));
}

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('J2bulkorder', JPATH_COMPONENT_ADMINISTRATOR);
JLoader::register('J2bulkorderHelper', JPATH_COMPONENT_ADMINISTRATOR . DIRECTORY_SEPARATOR . 'helpers' . DIRECTORY_SEPARATOR . 'j2bulkorder.php');

$controller = JControllerLegacy::getInstance('J2bulkorder');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
