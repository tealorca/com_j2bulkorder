<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_J2bulkorder
 * @author     JooMonk <support@joomonk.com>
 * @copyright  JooMonk
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Sate controller class.
 *
 * @since  1.6
 */
class J2bulkorderControllerSate extends JControllerForm
{
	/**
	 * Constructor
	 *
	 * @throws Exception
	 */
	public function __construct()
	{
		$this->view_list = 'sates';
		parent::__construct();
	}
}
