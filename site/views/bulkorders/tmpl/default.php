<?php
/**
 * @package J2Store
 * @copyright Copyright (c)2014-17 Ramesh Elamathi / J2Store.org
 * @license GNU GPL v3 or later
 */
// No direct access
defined('_JEXEC') or die;
JHTML::_('behavior.modal');
$this->params = J2Store::config();
?>
<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th width="20%" >Image</th>
			<th width="20%" >Name</th>
			<th width="20%" >Categories</th>
			<th width="20%">Price </th>

			<th width="20%">Buy</th>
			
		</tr>
	</thead>

	<tbody>
		<form action="<?php echo JURI::base()."index.php?option=com_j2bulkorder&task=addToCart" ?>" method="post" id="form1">
		<?php foreach ($this->items as $product) { ?>
		<tr>
			<td> </td>
			<td><?php echo $product->source->title ?> </td>
			<td><?php echo $product->source->category_title ?> </td>
			<td> </td>
			<td><input type="text" class="" style="width:50%; float: left; display: inline;" name="<?php echo $product->j2store_product_id;?>[qty]" />
 				<input type="checkbox" class="" style="float: right; display: inline; width:50%;"  name="<?php echo $product->j2store_product_id;?>[check]" value="null" />	
 			</td>
		</tr>
		<?php } ?>
		 <button type="submit" form="form1" value="Submit">Submit</button> 
	</form>
	</tbody>
	
</table>