<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_J2bulkorder
 * @author     JooMonk <support@joomonk.com>
 * @copyright  JooMonk
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('J2bulkorder', JPATH_COMPONENT);
JLoader::register('J2bulkorderController', JPATH_COMPONENT . '/controller.php');


// Execute the task.
$controller = JControllerLegacy::getInstance('J2bulkorder');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
