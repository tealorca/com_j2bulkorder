<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_J2bulkorder
 * @author     JooMonk <support@joomonk.com>
 * @copyright  JooMonk
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controller');

/**
 * Class J2bulkorderController
 *
 * @since  1.6
 */
class J2bulkorderController extends JControllerLegacy
{
	/**
	 * Method to display a view.
	 *
	 * @param   boolean $cachable  If true, the view output will be cached
	 * @param   mixed   $urlparams An array of safe url parameters and their variable types, for valid values see {@link JFilterInput::clean()}.
	 *
	 * @return  JController   This object to support chaining.
	 *
	 * @since    1.5
	 */
	public function display($cachable = false, $urlparams = false)
	{
        $app  = JFactory::getApplication();
        $view = $app->input->getCmd('view', 'sates');
		$app->input->set('view', $view);

		parent::display($cachable, $urlparams);

		return $this;
	}
	public function addToCart(){
		//Get post inputs 
        $app  = JFactory::getApplication();
		if (!defined('F0F_INCLUDED'))
		{
			include_once JPATH_ADMINISTRATOR . '/components/com_j2store/fof/include.php';
		}
		if (!class_exists('J2Store'))
		{
			jimport('joomla.filesystem.file');
			if (JFile::exists(JPATH_ADMINISTRATOR . '/components/com_j2store/helpers/j2store.php'))
			{
				require_once(JPATH_ADMINISTRATOR . '/components/com_j2store/helpers/j2store.php');
			}
		}
		$product_array=json_decode(file_get_contents("php://input"), true);
		foreach ($product_array['multi_cart_items'] as $key => $value) {				
			$qty=$value[0]['qty'];
			$product_id= $value[0]['product_id'];			
			$product = \J2Store::product()->setId($product_id)->getProduct();
			\F0FModel::getTmpInstance('Products', 'J2StoreModel')->runMyBehaviorFlag(true)->getProduct($product);
			$cart_model = \F0FModel::getAnInstance('Carts', 'J2StoreModel');
			$item                  = new \JObject;
			$item->user_id         = \JFactory::getUser()->id;
			$item->product_id      = (int) $product_id;
			$item->variant_id      = (int) $product->variants->j2store_variant_id;
			$item->product_qty     = (int) $qty;
			$item->product_options = base64_encode(serialize($options));
			$item->product_type    = $product->product_type;
			$item->vendor_id       = isset($product->vendor_id) ? $product->vendor_id : '0';	
			$cartTable = $cart_model->addItem($item);
		}
	}
}
