<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_J2bulkorder
 * @author     JooMonk <support@joomonk.com>
 * @copyright  JooMonk
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

/**
 * Sates list controller class.
 *
 * @since  1.6
 */
class J2bulkorderControllerSates extends J2bulkorderController
{
	/**
	 * Proxy for getModel.
	 *
	 * @param   string  $name    The model name. Optional.
	 * @param   string  $prefix  The class prefix. Optional
	 * @param   array   $config  Configuration array for model. Optional
	 *
	 * @return object	The model
	 *
	 * @since	1.6
	 */
	public function &getModel($name = 'Sates', $prefix = 'J2bulkorderModel', $config = array())
	{
		$model = parent::getModel($name, $prefix, array('ignore_request' => true));

		return $model;
	}
}
